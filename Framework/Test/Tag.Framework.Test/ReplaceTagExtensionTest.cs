using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace Tag.Framework.Test
{
    [ExcludeFromCodeCoverage]
    public class ReplaceTagExtensionTest
    {
        [Fact]
        public void ShouldReplaceTagWithValue()
        {
            var taggedString = "https://www.google.com/search?q=<KeyWord>&num=100";
            var replaceValue = "e-settlement";

            var replacedString = taggedString.ReplaceTag("KeyWord", replaceValue);

            Assert.Equal("https://www.google.com/search?q=e-settlement&num=100", replacedString);
        }
    }
}
