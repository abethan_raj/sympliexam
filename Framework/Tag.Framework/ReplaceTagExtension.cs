﻿using System.Text.RegularExpressions;

namespace Tag.Framework
{
    public static class ReplaceTagExtension
    {
        public static string ReplaceTag(this string text, string tag, string value)
        {
            return Regex.Replace(text, $"<{tag}>", value);
        }
    }
}
