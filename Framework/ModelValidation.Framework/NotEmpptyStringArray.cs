﻿using System.Collections;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ModelValidation.Framework
{
    public class NotEmpptyStringArray : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value.GetType().IsAssignableFrom(typeof(string[])))
            {
                var hasEmptyString = ((IEnumerable)value).Cast<object>()
                  .Select(x => x == null ? string.Empty : x.ToString())
                  .FirstOrDefault(x => x.Length == 0) != null;

                if (hasEmptyString)
                {
                    return new ValidationResult("String array shouldn't contain empty strings");
                }

                return ValidationResult.Success;
            }

            return new ValidationResult("Unsupported type");
        }
    }
}