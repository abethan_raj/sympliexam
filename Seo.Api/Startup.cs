using System.Net.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Seo.Api.Contract.SettingModels;
using Seo.Api.ServiceLogic;

namespace Seo.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var searchEngineSettings = new SearchEngineSettings();
            Configuration.GetSection(nameof(SearchEngineSettings)).Bind(searchEngineSettings);

            services.TryAddSingleton(searchEngineSettings);
            services.TryAddSingleton(new HttpClient());


            foreach (var searchEngine in searchEngineSettings.SearchEngines)
            {
                services.AddSingleton<IWebPageRegexProcessorService>(_ =>
                     new WebPageRegexProcessorService(_.GetService<HttpClient>(), searchEngine.Name, searchEngine.SearchUrl, searchEngine.RegexPattern));
            }

            services.TryAddSingleton<ISearchEngineSearchService, SearchEngineSearchService>();
            services.TryAddSingleton<ICacheSearchEngineSearchService, CacheSearchEngineSearchService>();

            services.AddMemoryCache();
            services.AddControllers();
            services.AddSwaggerGen(x => x.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "SEO", Version = "v1" }));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSwagger();
            app.UseSwaggerUI(x => x.SwaggerEndpoint("/swagger/v1/swagger.json", "SEO API Version 1"));

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints => endpoints.MapControllers());
        }
    }
}
