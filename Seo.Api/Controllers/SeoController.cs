﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Seo.Api.Contract.ViewModels;
using Seo.Api.ServiceLogic;

namespace Seo.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SeoController : ControllerBase
    {
        private readonly ISearchEngineSearchService _searchEngineSearchService;

        public SeoController(ISearchEngineSearchService searchEngineSearchService)
        {
            _searchEngineSearchService = searchEngineSearchService;
        }

        /// <summary>
        /// Get SEO positions for the keywords
        /// </summary>
        /// <param name="getUrlPositionRequest">GetUrlPositionRequest</param>
        /// <returns>GetUrlPositionsResponse</returns>
        [HttpGet]
        public async Task<GetUrlPositionsResponse> Get([FromQuery] GetUrlPositionRequest getUrlPositionRequest)
        {
            try
            {
                return await _searchEngineSearchService.GetUrlPosition(getUrlPositionRequest);
            }
            catch
            {
                throw;
            }
        }
    }
}
