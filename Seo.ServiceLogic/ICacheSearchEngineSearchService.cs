﻿using System.Threading.Tasks;

namespace Seo.Api.ServiceLogic
{
    public interface ICacheSearchEngineSearchService
    {
        Task<int> GetUrlPosition(string keyword, string searchEngineName, string searchUrl);
    }
}
