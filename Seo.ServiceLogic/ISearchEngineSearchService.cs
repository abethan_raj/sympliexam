﻿using System.Threading.Tasks;
using Seo.Api.Contract.ViewModels;

namespace Seo.Api.ServiceLogic
{
    public interface ISearchEngineSearchService
    {
        Task<GetUrlPositionsResponse> GetUrlPosition(GetUrlPositionRequest getUrlPositionRequest);
    }
}
