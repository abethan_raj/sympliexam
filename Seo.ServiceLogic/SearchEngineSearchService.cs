﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Seo.Api.Contract.SettingModels;
using Seo.Api.Contract.ViewModels;

namespace Seo.Api.ServiceLogic
{
    public class SearchEngineSearchService : ISearchEngineSearchService
    {
        private readonly ICacheSearchEngineSearchService _cacheSearchEngineSearchService;
        private readonly IMemoryCache _memoryCache;

        public SearchEngineSettings _searchEngineSettings { get; }

        public SearchEngineSearchService(ICacheSearchEngineSearchService cacheSearchEngineSearchService, IMemoryCache memoryCache, SearchEngineSettings searchEngineSettings)
        {
            _cacheSearchEngineSearchService = cacheSearchEngineSearchService;
            _memoryCache = memoryCache;
            _searchEngineSettings = searchEngineSettings;
        }

        public virtual async Task<GetUrlPositionsResponse> GetUrlPosition(GetUrlPositionRequest getUrlPositionRequest)
        {
            var getUrlPositionsResponse = new GetUrlPositionsResponse
            {
                SearchEngineUrlPositions = new List<SearchEngineUrlPosition>()
            };

            foreach (var searchEngine in _searchEngineSettings.SearchEngines)
            {
                var keywordPositions = new List<KeywordPosition>();

                foreach (var keyword in getUrlPositionRequest.Keywords)
                {
                    keywordPositions.Add(new KeywordPosition { Keyword = keyword, Position = await _cacheSearchEngineSearchService.GetUrlPosition(keyword, searchEngine.Name, getUrlPositionRequest.SearchUrl) });
                }

                getUrlPositionsResponse.SearchEngineUrlPositions.Add(new SearchEngineUrlPosition { SearchEngineName = searchEngine.Name, KeywordPositions = keywordPositions });
            }

            return getUrlPositionsResponse;
        }
    }
}
