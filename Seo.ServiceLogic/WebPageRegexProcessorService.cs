﻿using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Tag.Framework;

namespace Seo.Api.ServiceLogic
{
    public interface IWebPageRegexProcessorService
    {
        Task<int> Process(string keyword, string searchUrl);
        string SearchEngine { get; }
    }

    public class WebPageRegexProcessorService : IWebPageRegexProcessorService
    {
        public string SearchEngine { get; }

        private readonly HttpClient _httpClient;
        private readonly string _navigateUrl;
        private readonly string _regexPattern;

        public WebPageRegexProcessorService(HttpClient httpClient, string searchEngineName, string navigateUrl, string regexPattern)
        {
            _httpClient = httpClient;
            _navigateUrl = navigateUrl;
            _regexPattern = regexPattern;

            SearchEngine = searchEngineName;
        }

        public async Task<int> Process(string keyword, string searchUrl)
        {
            var keywordNavigateUrl = _navigateUrl.ReplaceTag("Keyword", keyword);
            var html = await _httpClient.GetStringAsync(keywordNavigateUrl);
            var matches = Regex.Matches(html, _regexPattern);

            foreach (var match in matches.Select((value, index) => new { index, value }))
            {
                if (match.value.ToString().Contains(searchUrl))
                {
                    return match.index + 1;
                }
            }

            return 0;
        }
    }
}
