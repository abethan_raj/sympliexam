﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Seo.Api.Contract.SettingModels;

namespace Seo.Api.ServiceLogic
{
    public class CacheSearchEngineSearchService : ICacheSearchEngineSearchService
    {
        private readonly IEnumerable<IWebPageRegexProcessorService> _webPageRegexProcessorServices;
        private readonly SearchEngineSettings _searchEngineSettings;
        private readonly IMemoryCache _memoryCache;

        public CacheSearchEngineSearchService(IEnumerable<IWebPageRegexProcessorService> webPageRegexProcessorServices, SearchEngineSettings searchEngineSettings, IMemoryCache memoryCache)
        {
            _webPageRegexProcessorServices = webPageRegexProcessorServices;
            _searchEngineSettings = searchEngineSettings;
            _memoryCache = memoryCache;
        }

        public async Task<int> GetUrlPosition(string keyword, string searchEngineName, string searchUrl)
        {
            _memoryCache.TryGetValue(GetMemoryKey(keyword, searchEngineName, searchUrl), out int? cachedPosition);
            return cachedPosition == null ? await GetPosition(keyword, searchEngineName, searchUrl) : cachedPosition.Value;
        }

        public virtual async Task<int> GetPosition(string keyword, string searchEngineName, string searchUrl)
        {
            var webPageRegexProcessorService = _webPageRegexProcessorServices.FirstOrDefault(x => x.SearchEngine == searchEngineName);
            if (webPageRegexProcessorService == null)
            {
                throw new ArgumentNullException("Search engine specific WebPageRegexProcessorSericeNotFond.");
            }

            var position = await webPageRegexProcessorService.Process(keyword, searchUrl);

            _memoryCache.Set(GetMemoryKey(keyword, searchEngineName, searchUrl), position, new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(_searchEngineSettings.CacheAbsoluteExpirationInMin)));
            return position;
        }

        private static string GetMemoryKey(string keyword, string searchEngineName, string searchUrl)
        {
            return keyword + searchEngineName + searchUrl;
        }
    }
}
