﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Moq;
using Seo.Api.Contract.SettingModels;
using Xunit;

namespace Seo.Api.ServiceLogic.Test
{
    [ExcludeFromCodeCoverage]
    public class SearchEngineSearchServiceTest
    {
        [Fact]
        public async Task InSakeOfTDD()
        {
            var cacheSearchEngineSearchService = new Mock<ICacheSearchEngineSearchService>();
            var memoryCache = MockMemoryCacheService.GetMemoryCache(null);

            var searchEngineSearchService = new SearchEngineSearchService(cacheSearchEngineSearchService.Object, memoryCache, new SearchEngineSettings
            {
                CacheAbsoluteExpirationInMin = 1,
                SearchEngines = new List<SearchEngine>
                {
                    new SearchEngine()
                    {
                        Name = "Google",
                        RegexPattern = "<div class=\"ZINbbc xpd O9g5cc uUPGi\"><div class=\"kCrYT\"><a href=\"([^\"]+)",
                        SearchUrl = "https://www.google.com/search?q=<Keyword>&num=100"
                    }
                }
            });

            var getUrlPositionsResponse = await searchEngineSearchService.GetUrlPosition(new Api.Contract.ViewModels.GetUrlPositionRequest { Keywords = new List<string> { "e-settlement" }.ToArray(), SearchUrl = "www.abethan.com" });

            Assert.Equal(0, getUrlPositionsResponse.SearchEngineUrlPositions[0].KeywordPositions[0].Position);
        }
    }
}