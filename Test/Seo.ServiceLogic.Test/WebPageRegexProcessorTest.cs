using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using Moq.Protected;
using Xunit;

namespace Seo.Api.ServiceLogic.Test
{
    [ExcludeFromCodeCoverage]
    public class WebPageRegexProcessorTest
    {
        [InlineData("https://www.sympli.com.au", 3)]
        [InlineData("testUrl", 0)]
        [Theory]
        public async Task ShouldReturnContainsIndexWithSearchUrl(string searchUrl, int searchEngineIndexExpected)
        {
            var mockHttpMessageHandler = new Mock<HttpMessageHandler>();

            mockHttpMessageHandler.Protected()
              .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
              .Returns(() =>
              {
                  var testDataFile = $"{Directory.GetCurrentDirectory()}/TestData/TestGooglePage.html";

                  var responseContent = File.ReadAllText(testDataFile);
                  var response = new HttpResponseMessage
                  {
                      StatusCode = HttpStatusCode.OK,
                      Content = new StringContent(responseContent)
                  };

                  return Task.FromResult(response);
              });

            var httpClient = new HttpClient(mockHttpMessageHandler.Object);
            var webPageXPathProcessor = new WebPageRegexProcessorService(httpClient, "Google", "https://www.bing.com/search?q=<Keyword>&count=100", "<div class=\"ZINbbc xpd O9g5cc uUPGi\"><div class=\"kCrYT\"><a href=\"([^\"]+)");
            var seachEngineIndex = await webPageXPathProcessor.Process("e-settlement", searchUrl);

            Assert.Equal(searchEngineIndexExpected, seachEngineIndex);
        }
    }
}
