using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Moq;
using Seo.Api.Contract.SettingModels;
using Xunit;

namespace Seo.Api.ServiceLogic.Test
{
    [ExcludeFromCodeCoverage]
    public class CachedSearchEngineSearchServiceTest
    {
        [Theory]
        [ClassData(typeof(GetUrlPositionsResponseTestData))]
        public async Task ShouldHitSearchEnginesSearchServiceAsPerTheExpectedMethodHitCount(int? cacheOutput, int expectedMethodHitCount)
        {
            var memoryCache = MockMemoryCacheService.GetMemoryCache(cacheOutput);

            var webPageRegexProcessorService = new Mock<IWebPageRegexProcessorService>();

            var cacheSearchEngineSearchService = new Mock<CacheSearchEngineSearchService>(new List<IWebPageRegexProcessorService> { webPageRegexProcessorService.Object }, new SearchEngineSettings { CacheAbsoluteExpirationInMin = 60 }, memoryCache);
            cacheSearchEngineSearchService.Setup(x => x.GetPosition(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(1));

            var position = await cacheSearchEngineSearchService.Object.GetUrlPosition("e-settlement", "Google", "www.sympli.com");

            cacheSearchEngineSearchService.Verify(x => x.GetPosition("e-settlement", "Google", "www.sympli.com"), Times.Exactly(expectedMethodHitCount));
        }
    }

    public class GetUrlPositionsResponseTestData : IEnumerable<object[]>
    {
        public virtual IEnumerator<object[]> GetEnumerator()
        {
            //Without cache
            yield return new object[] { null, 1 };

            //With cache value 1
            yield return new object[]
            {
                1, 0
            };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
