﻿using System.ComponentModel.DataAnnotations;
using ModelValidation.Framework;

namespace Seo.Api.Contract.ViewModels
{
    public class GetUrlPositionRequest
    {
        [MinLength(1)]
        [Required]
        [NotEmpptyStringArray]
        public string[] Keywords { get; set; }

        [Required]
        [RegularExpression(@"(www\.)(\w+\.\w{2,3})+(\.\w+)?(\/.*)*")]
        public string SearchUrl { get; set; }
    }
}