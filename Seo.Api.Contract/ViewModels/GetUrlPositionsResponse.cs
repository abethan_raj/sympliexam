﻿using System.Collections.Generic;

namespace Seo.Api.Contract.ViewModels
{
    public class GetUrlPositionsResponse
    {
        public List<SearchEngineUrlPosition> SearchEngineUrlPositions { get; set; }
    }
}
