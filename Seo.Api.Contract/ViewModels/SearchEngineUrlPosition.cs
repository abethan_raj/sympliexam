﻿using System.Collections.Generic;

namespace Seo.Api.Contract.ViewModels
{
    public class SearchEngineUrlPosition
    {
        public string SearchEngineName { get; set; }
        public List<KeywordPosition> KeywordPositions { get; set; }
    }
}
