﻿namespace Seo.Api.Contract.ViewModels
{
    public class KeywordPosition
    {
        public string Keyword { get; set; }
        public int Position { get; set; }
    }
}
