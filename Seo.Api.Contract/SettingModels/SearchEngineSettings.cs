﻿using System.Collections.Generic;

namespace Seo.Api.Contract.SettingModels
{
    public class SearchEngineSettings
    {
        public List<SearchEngine> SearchEngines { get; set; }
        public int CacheAbsoluteExpirationInMin { get; set; }
    }
}