﻿namespace Seo.Api.Contract.SettingModels
{
    public class SearchEngine
    {
        public string Name { get; set; }
        public string SearchUrl { get; set; }
        public string RegexPattern { get; set; }
    }
}
